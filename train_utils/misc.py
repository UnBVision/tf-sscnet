import os
import pandas as pd

def get_run():
    if os.path.exists("runcount.csv"):
        rc = pd.read_csv("runcount.csv")
    else:
        rc=pd.DataFrame(data={'run':['0']}, dtype='int')
    run = rc.run[0]
    run += 1
    rc.run = [run]
    rc.to_csv("runcount.csv", index=False)
    return run

