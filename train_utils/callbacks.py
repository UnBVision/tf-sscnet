from keras.callbacks import Callback, EarlyStopping, ModelCheckpoint, ReduceLROnPlateau, TensorBoard
import keras.backend as K

def one_cycle_lr(iteration):
    """Given the inputs, calculates the lr that should be applicable for this iteration"""
    first_step = 10
    last_step = 80
    base_lr = 0.0003
    max_lr = 0.001
    min_lr = 0.000005
    if iteration < first_step:
        lr = base_lr + iteration*(max_lr-base_lr)/first_step
    else:
        lr = max_lr - (iteration-first_step)*(max_lr-min_lr)/(last_step-first_step)

    if lr<min_lr:
        lr = min_lr

    return lr


class log_lr(Callback):
    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        logs['lr'] = K.get_value(self.model.optimizer.lr)


def get_callbacks(model_name, log_dir="/tmp/tensor_board", patience=30):
    es = EarlyStopping(patience=patience, mode="min", monitor="val_loss")
    msave = ModelCheckpoint(model_name+'.hdf5', save_best_only=True, save_weights_only=True, mode="min", monitor="val_loss")
    log = TensorBoard(log_dir=log_dir+"/"+model_name)
    #red = ReduceLROnPlateau(patience=patience//5, verbose=1, cooldown=patience//6)
    return [es, msave, log, log_lr()]#, red]

