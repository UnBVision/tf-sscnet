# TF-SSCNet

### A Tensorflow-based implementation of a Semantic Scene Completion Network from a single depth image 
This is the repository of code porting of [SSCNet](https://github.com/shurans/sscnet) 
from Caffe to Tensorflow. It also includes some memory and execution time optimizations.

![teaser](teaser.png)

## Contents
1. [Organization](#organization)
2. [Requirements](#requirements)
3. [Compiling](#compiling)
4. [Demo Application](#demoapplication)
5. [Training](#training)
6. [Evaluating](#evaluating)

## Organization 
The project folder is organized as follows:
``` shell
    tf-sscnet
         |-- lib_sscnet            //py-cuda integration and other sscnet py libraries
         |-- results               //training results
         |-- sample_images         //sample scenes  
         |-- src                   //cuda source code  
         |-- train_utils           //.py libraries for training
         |-- weights               //sample pre-trained weights
```

## Requirements 
1. Hardware Requirements (minimum):

   * For training : a 11G GPU (for example, a GTX 1080TI) in order to train with a batch-size of 4 scenes
   * For inference: a 4G GPU (for example, a GTX 1050TI) 

2. Software Requirements:
 
   * Python 3
   * Tensorflow with GPU/CUDA support: installation in a virtual environment like 
   [Pip/Conda](https://www.tensorflow.org/install/pip) or 
   [Docker](https://www.tensorflow.org/install/docker) is recommended.   
   * Keras
   * OpenCV

## Compiling
``` shell
    cd src
    nvcc --ptxas-options=-v --compiler-options '-fPIC' -o lib_sscnet.so --shared lib_sscnet.cu
```
         
## Demo Application
This demo uses one of the provided pre-trained networks to make predictions given a depth image. 
Simple visualizations are available. 

Two weight files are provided. The first one is trained over SunCG synthetic data and 
the other is fine tuned over NYU-v2 dataset (real images gathered with Kinect). The default weight file
is the NYU-v2 fine tuned.

Sample depth images images are provided under ./sample_images directory. This is the default image 
directory for this demo script. Bin files are provided only for visualizations purposes.

You can freely mix SunCG and NYU images and weights, but results are better when you use weights and images 
from the same source. 

If you have more than one GPU in your system you can choose the device to be used changing DEVICE 
global variable in the beginning of demo.py file. With a little code change it is possible to
use one GPU to pre-process and other GPU for inference.       


#### 1. Getting help:

``` shell

python demo.py -h

usage: demo.py [-h] [--image_dir IMAGE_DIR] [--weights_dir WEIGHTS_DIR]
               [--weights_file WEIGHTS_FILE] [--show_pred {y,Y,n,N}]
               [--show_gt {y,Y,n,N}]
```

#### 2. Predicting over a NYU sample depth image using NYU-v2 fine tuned weights and showing GT:

``` shell

python demo.py NYU0079_0000 --show_gt=y
```

#### 3. Predicting over a SunCG sample depth image using SunCG synthetic data pre-trained weigths and showing GT:

``` shell

python demo.py SUNCGtest_00000028 --show_gt=y --weights_file=tf-sscnet_suncg.hdf5
```

## Training
The training process consists of pre-training the tf-sscnet network using SunCG Dataset synthetic data,
then fine tuning using real images from NYU. We provide an already trained network, but if you want to train 
your own network, follow these steps.

#### 1. Getting training datasets: 

The original SSCNET project provides voxelized ground and standard test splits for both SunCG and 
NYU-v2 datasets. In order to train the tf-sscnet network from scratch you need to download 
the training datasets using scripts download_data.sh and download_suncgTrain.sh available [here](https://github.com/shurans/sscnet). 

After download, your data folder should look like this:
``` shell
         <data_root>
                 |-- data
                        |-- depthbin
                            |-- NYUtrain 
                                |-- xxxxx_0000.png
                                |-- xxxxx_0000.bin
                            |-- NYUtest
                            |-- NYUCADtrain
                            |-- NYUCADtest
                            |-- SUNCGtest_49700_49884
                        |-- eval                 //matlab files used for evaluation
                            |-- NYUtest
                            |-- NYUCADtest
                            |-- SUNCGtest_49700_49884
                        |-- SUNCGtrain
                            |-- SUNCGtrain_1_500    
                            |-- SUNCGtrain_501_1000    
                            |-- SUNCGtrain_1001_2000    
                            ...  
```
     
#### 2. Pre-processing flipped TSDF encoding

The process of generating F-TSDF encoding is very time consuming. We provide a way to 
pre-process F-TSDF of training data in order to make the training pipeline lighter.

Getting help
``` shell
         python preproc_sscnet.py -h
```


Preprocessing SUNCG training data:
``` shell
         python preproc_sscnet.py SUNCGtrain --base_path <data_root>/data --dest_path <destination> 
```

Preprocessing NYU fine tune data:
``` shell
         python preproc_sscnet.py NYUtrain --base_path <data_root>/data/depthbin --dest_path <destination> 
```
       
#### 3. Training and fine tuning 
Training from preprocessed SUNCG Synthetic data  (you may need to adjust paths in code) 

``` shell
         python train_sscnet_preproc.py
```

Fine tuning from preprocessed NYU data  (you may need to adjust paths in code)
``` shell
         python finetune_sscnet.py 
```
## Evaluating 


Evaluation follows protocol defined in SSCNET paper. Our scripts uses 3D volume information like
occlusions, FOV, and out-of-room areas provided im Matlab files that should be at 
<data_root>/data/eval folder. You should provide the weight file to be evaluated. You also may need 
to adjust the paths directly in the code.  

#### 1. Evaluating on SUNCG test set 


``` shell
         python evaluate_SUNCG.py tf-sscnet_suncg.hdf5
```


#### 2. Evaluating on NYU test set 


``` shell
         python evaluate_NYU.py tf-sscnet_nyu.hdf5
```

