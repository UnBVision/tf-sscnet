from keras.models import Model
from keras.layers import Input, concatenate, Conv3D, Add, MaxPooling3D, Activation, Reshape, BatchNormalization
from keras.layers import UpSampling3D
from keras.initializers import RandomNormal



def get_sscnet():
    input = Input(shape=(240, 144, 240, 1)) #channels last

    # Conv1

    conv1 = Conv3D(16, 7, strides=2, dilation_rate=1, padding='same', name='conv_1_1', activation='relu')(input)
    conv1 = Conv3D(32, 3, strides=1, dilation_rate=1, padding='same', name='conv_1_2', activation='relu')(conv1)
    conv1 = Conv3D(32, 3, strides=1, dilation_rate=1, padding='same', name='conv_1_3')(conv1)

    add1 = Conv3D(32, 1, strides=1, dilation_rate=1, padding='same', name='red_1')(conv1) #reduction
    add1 = Add()([conv1, add1])
    add1 = Activation('relu')(add1)

    pool1 = MaxPooling3D(2, strides=2)(add1)

    # Conv2

    conv2 = Conv3D(64, 3, strides=1, dilation_rate=1, padding='same', name='conv_2_1', activation='relu')(pool1)
    conv2 = Conv3D(64, 3, strides=1, dilation_rate=1, padding='same', name='conv_2_2', activation='relu')(conv2)

    add2 = Conv3D(64, 1, strides=1, dilation_rate=1, padding='same', name='red_2')(pool1) #reduction
    add2 = Add()([conv2, add2])
    add2 = Activation('relu')(add2)
    add2 = Activation('relu')(add2) # 2 ativações?

    # Conv3

    conv3 = Conv3D(64, 3, strides=1, dilation_rate=1, padding='same', name='conv_3_1', activation='relu')(add2)
    conv3 = Conv3D(64, 3, strides=1, dilation_rate=1, padding='same', name='conv_3_2', activation='relu')(conv3)

    add3 = Add()([conv3, add2])
    add3 = Activation('relu')(add3)
    add3 = Activation('relu')(add3) # 2 ativações?

    # Dilated1

    dil1 = Conv3D(64, 3, strides=1, dilation_rate=2, padding='same', name='dil_1_1', activation='relu')(add3)
    dil1 = Conv3D(64, 3, strides=1, dilation_rate=2, padding='same', name='dil_1_2', activation='relu')(dil1)

    add4 = Add()([dil1, add3])
    add4 = Activation('relu')(add4)
    add4 = Activation('relu')(add4)

    # Dilated2

    dil2 = Conv3D(64, 3, strides=1, dilation_rate=2, padding='same', name='dil_2_1', activation='relu')(add4)
    dil2 = Conv3D(64, 3, strides=1, dilation_rate=2, padding='same', name='dil_2_2', activation='relu')(dil2)

    add5 = Add()([dil2, add4])
    add5 = Activation('relu')(add5)

    # Concat

    conc = concatenate([add2, add3, add4, add5], axis=4)

    # Final Convolutions

    init1 = RandomNormal(mean=0.0, stddev=0.01, seed=None)
    fin = Conv3D(128, 1, padding='same', name='fin_1', activation='relu', kernel_initializer=init1 )(conc)

    init2 = RandomNormal(mean=0.0, stddev=0.01, seed=None)
    fin = Conv3D(128, 1, padding='same', name='fin_2', activation='relu', kernel_initializer=init2 )(fin)

    init3 = RandomNormal(mean=0.0, stddev=0.01, seed=None)
    fin = Conv3D(12, 1, padding='same', name='fin_3', activation='softmax', kernel_initializer=init3 )(fin)

    model = Model(inputs=input, outputs=fin)

    return model

def get_usscnet():
    input_tsdf = Input(shape=(240, 144, 240, 1)) #channels last


    down1 = Conv3D(8, (3, 3, 3), padding='same')(input_tsdf)
    down1 = BatchNormalization()(down1)
    down1 = Activation('relu')(down1)
    down1 = Conv3D(8, (3, 3, 3), padding='same')(down1)
    down1 = BatchNormalization()(down1)
    down1 = Activation('relu')(down1)
    down1_pool = MaxPooling3D((2, 2, 2), strides=(2, 2, 2))(down1)
    # 120 x 72 x 120

    down2 = Conv3D(16, (3, 3, 3), padding='same')(down1_pool)
    down2 = BatchNormalization()(down2)
    down2 = Activation('relu')(down2)
    down2 = Conv3D(16, (3, 3, 3), padding='same')(down2)
    down2 = BatchNormalization()(down2)
    down2 = Activation('relu')(down2)
    down2_pool = MaxPooling3D((2, 2, 2), strides=(2, 2, 2))(down2)
    # 60 x 36 x 60

    down3 = Conv3D(32, (3, 3, 3), padding='same')(down2_pool)
    down3 = BatchNormalization()(down3)
    down3 = Activation('relu')(down3)
    down3 = Conv3D(32, (3, 3, 3), padding='same')(down3)
    down3 = BatchNormalization()(down3)
    down3 = Activation('relu')(down3)
    down3_pool = MaxPooling3D((2, 2, 2), strides=(2, 2, 2))(down3)
    # 30 x 18 x 30

    down4 = Conv3D(64, (3, 3, 3), padding='same', dilation_rate=2)(down3_pool)
    down4 = BatchNormalization()(down4)
    down4 = Activation('relu')(down4)
    down4 = Conv3D(64, (3, 3, 3), padding='same', dilation_rate=2)(down4)
    down4 = BatchNormalization()(down4)
    down4 = Activation('relu')(down4)
    down4_pool = MaxPooling3D((2, 2, 2), strides=(2, 2, 2))(down4)
    # 15 x 9 x 15

    center = Conv3D(128, (3, 3, 3), padding='same', dilation_rate=2)(down4_pool)
    center = BatchNormalization()(center)
    center = Activation('relu')(center)
    center = Conv3D(128, (3, 3, 3), padding='same', dilation_rate=2)(center)
    center = BatchNormalization()(center)
    center = Activation('relu')(center)
    # center

    up4 = UpSampling3D((2, 2, 2))(center)
    up4 = concatenate([down4, up4], axis=-1)
    up4 = Conv3D(64, (3, 3, 3), padding='same')(up4)
    up4 = BatchNormalization()(up4)
    up4 = Activation('relu')(up4)
    up4 = Conv3D(64, (3, 3, 3), padding='same')(up4)
    up4 = BatchNormalization()(up4)
    up4 = Activation('relu')(up4)
    up4 = Conv3D(164, (3, 3, 3), padding='same')(up4)
    up4 = BatchNormalization()(up4)
    up4 = Activation('relu')(up4)
    # 30 x 18 x 30

    up3 = UpSampling3D((2, 2, 2))(up4)
    up3 = concatenate([down3, up3], axis=-1)
    up3 = Conv3D(32, (3, 3, 3), padding='same')(up3)
    up3 = BatchNormalization()(up3)
    up3 = Activation('relu')(up3)
    up3 = Conv3D(32, (3, 3, 3), padding='same')(up3)
    up3 = BatchNormalization()(up3)
    up3 = Activation('relu')(up3)
    up3 = Conv3D(32, (3, 3, 3), padding='same')(up3)
    up3 = BatchNormalization()(up3)
    up3 = Activation('relu')(up3)
    # 60 x 36 60

    fin = concatenate([down2_pool, up3], axis=-1)
    fin = Conv3D(16, 1, padding='same', name='fin_1', activation='relu')(fin)
    fin = Conv3D(16, 1, padding='same', name='fin_2', activation='relu')(fin)
    fin = Conv3D(12, 1, padding='same', name='fin_3', activation='softmax' )(fin)

    model = Model(inputs=input_tsdf, outputs=fin)

    return model