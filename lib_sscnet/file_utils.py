from lib_sscnet.py_cuda import *
import numpy as np
from sklearn.utils import shuffle
from fnmatch import fnmatch
import os
from keras.utils import to_categorical
import threading

def get_file_prefixes_from_path(data_path, criteria="*.bin"):
    prefixes = []

    for path, subdirs, files in os.walk(data_path):
        for name in files:
            if fnmatch(name, criteria):
                prefixes.append(os.path.join(path, name)[:-4])

    return prefixes


class threadsafe_generator(object):
    """Takes an iterator/generator and makes it thread-safe by
    serializing call to the `next` method of given iterator/generator.
    """
    def __init__(self, gen):
        self.gen = gen
        self.lock = threading.Lock()

    def __iter__(self):
        return self

    def __next__(self):
        with self.lock:
            return self.gen.__next__()


def threadsafe(f):
    """A decorator that takes a generator function and makes it thread-safe.
    """
    def g(*a, **kw):
        return threadsafe_generator(f(*a, **kw))
    return g


@threadsafe
def ftsdf_generator(file_prefixes, batch_size=4, shuff=False, shape=(240, 144, 240), down_scale = 4):  # write the definition of your data generator

    while True:
        x_batch = np.zeros((batch_size, shape[0], shape[1], shape[2], 1))
        y_batch = np.zeros((batch_size, shape[0] // down_scale, shape[1] // down_scale, shape[2] // down_scale, 12))
        w_batch = np.zeros((batch_size, shape[0] // down_scale, shape[1] // down_scale, shape[2] // down_scale))
        batch_count = 0
        if shuff:
            file_prefixes_s = shuffle(file_prefixes)
        else:
            file_prefixes_s = file_prefixes

        for count, file_prefix in enumerate(file_prefixes_s):


            vox_tsdf, segmentation_label, vox_weights = process(file_prefix, voxel_shape=(240, 144, 240), down_scale=4)

            x_batch[batch_count] = vox_tsdf.reshape((shape[0], shape[1], shape[2],1))
            y_batch[batch_count] = to_categorical(segmentation_label.reshape((60, 36, 60,1)), num_classes=12)
            w_batch[batch_count] = vox_weights.reshape((60, 36, 60,1))
            batch_count += 1
            if batch_count == batch_size:
                yield x_batch, y_batch, w_batch
                x_batch = np.zeros((batch_size, shape[0], shape[1], shape[2], 1)) #channels last
                y_batch = np.zeros((batch_size, shape[0] // down_scale, shape[1] // down_scale, shape[2] // down_scale, 12))
                w_batch = np.zeros((batch_size, shape[0]//down_scale, shape[1]//down_scale, shape[2]//down_scale))
                batch_count = 0

        if(batch_count > 0):
            yield x_batch[:batch_count], y_batch[:batch_count],w_batch[:batch_count]


@threadsafe
def preproc_generator(file_prefixes, batch_size=4, shuff=False, shape=(240, 144, 240), down_scale = 4):  # write the definition of your data generator

    down_shape = (shape[0] // down_scale,  shape[1] // down_scale, shape[2] // down_scale)

    while True:
        x_batch = np.zeros((batch_size, shape[0], shape[1], shape[2], 1))
        y_batch = np.zeros((batch_size, down_shape[0], down_shape[1], down_shape[2], 12))
        #w_batch = np.zeros((batch_size, down_shape[0] * down_shape[1] * down_shape[2]* 12,))
        batch_count = 0
        if shuff:
            file_prefixes_s = shuffle(file_prefixes)
        else:
            file_prefixes_s = file_prefixes

        for count, file_prefix in enumerate(file_prefixes_s):

            #print(file_prefix)
            npz_file = file_prefix + '.npz'
            loaded = np.load(npz_file)

            vox_tsdf  = loaded['tsdf']
            vox_label  = loaded['lbl']
            vox_weights = loaded['weights']

            x_batch[batch_count] = vox_tsdf
            labels = to_categorical(vox_label, num_classes=12)
            weights =  np.repeat(vox_weights,12,axis=-1).reshape((down_shape[0], down_shape[1], down_shape[2], 12))
            y_batch[batch_count] = labels * (weights+1)
            batch_count += 1
            if batch_count == batch_size:
                yield x_batch, y_batch, #w_batch
                x_batch = np.zeros((batch_size, shape[0], shape[1], shape[2], 1)) #channels last
                y_batch = np.zeros((batch_size, down_shape[0], down_shape[1], down_shape[2], 12))
                #w_batch = np.zeros((batch_size, down_shape[0] * down_shape[1] * down_shape[2]* 12))
                batch_count = 0

        if batch_count > 0:
            yield x_batch[:batch_count], y_batch[:batch_count], #w_batch[:batch_count]
@threadsafe
def evaluate_generator(file_prefixes, batch_size=4, shuff=False, shape=(240, 144, 240), down_scale = 4):  # write the definition of your data generator

    while True:
        x_batch = np.zeros((batch_size, shape[0], shape[1], shape[2], 1))
        y_batch = np.zeros((batch_size, shape[0] // down_scale, shape[1] // down_scale, shape[2] // down_scale, 12))
        f_batch = np.zeros((batch_size, shape[0] // down_scale, shape[1] // down_scale, shape[2] // down_scale),dtype=np.int32)
        batch_count = 0
        if shuff:
            file_prefixes_s = shuffle(file_prefixes)
        else:
            file_prefixes_s = file_prefixes

        for count, file_prefix in enumerate(file_prefixes_s):


            vox_tsdf, segmentation_label, vox_flags = process_evaluate(file_prefix, voxel_shape=(240, 144, 240), down_scale=4)

            x_batch[batch_count] = vox_tsdf.reshape((shape[0], shape[1], shape[2],1))
            y_batch[batch_count] = to_categorical(segmentation_label.reshape((60, 36, 60,1)), num_classes=12)
            f_batch[batch_count] = vox_flags.reshape((60, 36, 60))
            batch_count += 1
            if batch_count == batch_size:
                yield x_batch, y_batch, f_batch
                x_batch = np.zeros((batch_size, shape[0], shape[1], shape[2], 1)) #channels last
                y_batch = np.zeros((batch_size, shape[0] // down_scale, shape[1] // down_scale, shape[2] // down_scale, 12))
                f_batch = np.zeros((batch_size, shape[0]//down_scale, shape[1]//down_scale, shape[2]//down_scale),dtype=np.int32)
                batch_count = 0

        if(batch_count > 0):
            yield x_batch[:batch_count], y_batch[:batch_count],f_batch[:batch_count]

