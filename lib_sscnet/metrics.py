import numpy as np
from keras import backend as K


def comp_iou(_y_true, _y_pred):
    smooth = K.epsilon()

    y_true = K.cast(K.greater_equal(_y_true, K.variable(1)),'float32')

    weights = K.max(_y_true-1, axis=-1)

    comp_weights = K.cast(
        K.not_equal(weights,K.variable(0.0))
        ,'float32')

    #print ("comp_weights", K.eval(comp_weights).shape)

    #print ("s comp_weights", K.eval(K.sum(comp_weights)))

    y_true = K.cast(K.clip(K.argmax(y_true),0,1),'float32')
    #print ("s y_true", K.eval(K.sum(y_true)))
    #print ("s y_true * w", K.eval(K.sum(y_true*comp_weights)))


    #print("comp y:\n", np.sum(K.eval(y_true)))
    y_pred = K.cast(K.clip(K.argmax(_y_pred),0,1),'float32')
    #print ("s y_pred", K.eval(K.sum(y_pred)))
    #print ("s y_pred * w", K.eval(K.sum(y_pred*comp_weights)))
    #print("comp p:\n", np.sum(K.eval(y_pred)))


    axis = (np.array(range(K.ndim(y_pred)-1))+1)*-1
    #print("axis",axis)

    inter = K.sum(y_true * y_pred * comp_weights, axis=axis)
    #print("comp inter:\n", K.eval(inter))
    union = K.sum(K.clip((y_true + y_pred) * comp_weights, 0, 1), axis=axis)
    #print("comp union:\n", K.eval(union))

    comp_iou = (inter) / (union + smooth)


    return comp_iou


def seg_iou(_y_true, _y_pred):
    smooth = K.epsilon()

    y_true = K.cast(K.greater_equal(_y_true, K.variable(1)),'float32')

    seg_weights = K.repeat_elements(K.max(_y_true-1, axis=-1, keepdims=True),12, axis=-1)


    #y_true = K.cast(K.argmax(y_true),'float32')
    #y_pred = K.cast(K.argmax(_y_pred),'float32')

    axis = (np.array(range(K.ndim(_y_pred)-1))+1)*-1

    inter = K.sum(y_true *_y_pred * seg_weights,axis=axis)
    union = K.sum(K.clip(y_true + _y_pred, 0.0, 1.0) * seg_weights,axis=axis)
    #print("seg union:\n", K.eval(union))


    seg_iou = (inter) / (union  + smooth)
    #print("seg iou:\n", K.eval(seg_iou))

    return seg_iou

def nclasses(_y_true, _y_pred):

    y_pred = K.cast(K.argmax(_y_pred),'float32')

    nc, idx = tf.unique(K.flatten(y_pred))

    nc = tf.size(nc)

    ret = nc

    return ret

def minclass(_y_true, _y_pred):

    y_pred = K.cast(K.argmax(_y_pred),'float32')

    nc, idx = tf.unique(K.flatten(y_pred))

    nc = K.min(nc)

    return nc

def maxclass(_y_true, _y_pred):

    y_pred = K.cast(K.argmax(_y_pred),'float32')

    nc, idx = tf.unique(K.flatten(y_pred))

    nc = K.max(nc)

    return nc

import tensorflow as tf


def comp_iou_np(_y_true, _y_pred, vol):
    #smooth = 0.00000001

    #only occluded
    flags=   np.array((vol<0)&(vol>=-1),dtype='float32')

    y_true = np.clip(np.argmax(_y_true, axis=-1),0,1) * flags

    y_pred = np.clip(np.argmax(_y_pred, axis=-1),0,1) * flags

    axis = (-1,-2,-3)

    inter = np.sum(y_true.flatten() * y_pred.flatten())
    union = np.sum(np.clip((y_true.flatten() + y_pred.flatten()), 0, 1))

    return inter, union


def seg_iou_np(_y_true, _y_pred, vol, cl):
    #smooth = 0.00000001

    #occluded and visible
    flags=   np.array((abs(vol)<1)|(vol==-1),dtype='float32')

    y_true = np.array(np.argmax(_y_true, axis=-1) == cl,dtype='float32') * flags
    y_pred = np.array(np.argmax(_y_pred, axis=-1) == cl,dtype='float32') * flags

    axis = (-1,-2,-3)

    inter = np.sum(y_true.flatten() * y_pred.flatten())
    union = np.sum(np.clip((y_true.flatten() + y_pred.flatten()), 0, 1))

    return inter, union


