import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"
os.environ["KERAS_BACKEND"]="tensorflow"
import sys

from lib_sscnet.file_utils import *
from lib_sscnet.metrics import *
from lib_sscnet.losses import *

import numpy as np # linear algebra
from keras.optimizers import SGD

from lib_sscnet.network import get_sscnet

import h5py

segmentation_class_map = np.array([0, 1, 2, 3, 4, 11, 5, 6, 7, 8, 8, 10, 10, 10, 11, 11, 9, 8, 11, 11,
                                   11, 11, 11, 11, 11, 11, 11, 10, 10, 11, 8, 10, 11, 9, 11, 11, 11], dtype=np.int32)



##################################

BATCH_SIZE = 1

LR = 0.01
#LR = 0.009
DECAY = 0.0005

voxel_shape = (240, 144, 240)

class_names = ["ceil.", "floor", "wall ", "wind.", "chair", "bed  ", "sofa ", "table", "tvs  ", "furn.", "objs."]

##################################

#lib_sscnet_setup(device=0, num_threads=128, K=None, frame_shape=(640, 480), v_unit=0.02, v_margin=0.24)

#data_path = '/d02/data/sscnet_preproc/SUNCGtest_49700_49884'
data_path = '/home/adn/sscnet/data/depthbin/NYUtest'
mat_path = '/home/adn/sscnet/data/eval/NYUtest'

file_prefixes = get_file_prefixes_from_path(data_path)

lib_sscnet_setup(device=0, num_threads=128, K=None, frame_shape=(640, 480), v_unit=0.02, v_margin=0.24)

eval_datagen = evaluate_generator(file_prefixes, batch_size=BATCH_SIZE, shuff=False, shape=(240, 144, 240))

model = get_sscnet()

#model.summary()

model.compile(optimizer=SGD(lr=LR, decay=DECAY,  momentum=0.9),
              loss=weighted_categorical_crossentropy
              ,metrics=[comp_iou, seg_iou]
              )



model_name = os.path.join('./weights',sys.argv[1])

#model.load_weights('sscnet_LR0.008_DC0.005_42.hdf5')
#model.load_weights('sscnet_LR0.01_DC0.0005_91_step17.hdf5')
model.load_weights(model_name)

print ("\nEvaluating model", model_name, "on NYU.")

comp_inter = 0
comp_union = 0

seg_inter = np.zeros((11,), dtype='float32')
seg_union = np.zeros((11,), dtype='float32')

max = int(np.ceil(len(file_prefixes)/BATCH_SIZE))

for round in range(int(np.ceil(len(file_prefixes)/BATCH_SIZE))):

    mat_prefix = file_prefixes[round][-12:]
    print(round,'/',max, mat_prefix, end="  ")

    f = h5py.File(mat_path+'/' + mat_prefix + '_gt_d4.mat', 'r')
    scene_vox = np.array(f['sceneVox_ds'])
    scene_vox[scene_vox==255] = 0;
    scene_vox[np.isnan(scene_vox)] = 0;
    label_obj = np.vectorize(lambda x: segmentation_class_map[int(x)])(scene_vox)

    f = h5py.File(mat_path+'/' + mat_prefix + '_vol_d4.mat', 'r')
    vol = np.array(f['flipVol_ds'])

    x, y, f = next(eval_datagen)

    pred = model.predict(x=x)
    pred_classes = np.argmax(pred, axis=-1)

    inter, union = comp_iou_np(y, pred, vol)
    comp_inter += inter
    comp_union += union
    for cl in range(0,11):
        inter, union = seg_iou_np(y, pred, vol, cl+1)
        seg_inter[cl] += inter
        seg_union[cl] += union

    print("Partial Comp IOU: %.1f     Seg IOU: %.1f" % (100 * comp_inter / comp_union, 100*np.mean((seg_inter+0.00001)/(seg_union+0.00001))), end='\r')


print("\nCompletion IOU: %.1f\n" %(100*comp_inter/comp_union))

for cl in range(0, 11):
    print("Class %s \tSegmentation IOU: %.1f" %(class_names[cl], 100 * seg_inter[cl] / seg_union[cl]))

print("\nMean Segmentation IOU: %.1f" %(100*np.mean(seg_inter/seg_union)))

