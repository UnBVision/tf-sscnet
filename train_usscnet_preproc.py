import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"
os.environ["KERAS_BACKEND"]="tensorflow"

from lib_sscnet.file_utils import *
from lib_sscnet.metrics import *
from lib_sscnet.losses import *
from train_utils.callbacks import *
from train_utils.misc import *

import numpy as np # linear algebra
from keras.optimizers import SGD

from sklearn.model_selection import train_test_split

from lib_sscnet.network import get_usscnet


##################################

BATCH_SIZE = 3
VAL_BATCH_SIZE = 1

LR = 0.01
#LR = 0.009
DECAY = 0.0005

voxel_shape = (240, 144, 240)

##################################

lib_sscnet_setup(device=0, num_threads=128, K=None, frame_shape=(640, 480), v_unit=0.02, v_margin=0.24)

data_path = '/d02/data/sscnet_preproc/SUNCGtrain'

file_prefixes = get_file_prefixes_from_path(data_path, criteria='*.npz')

train_prefixes, val_prefixes = train_test_split(file_prefixes, test_size=0.15, random_state=111)


train_datagen = preproc_generator(train_prefixes, batch_size=BATCH_SIZE, shuff=True, shape=(240, 144, 240))
val_datagen = preproc_generator(val_prefixes, batch_size=VAL_BATCH_SIZE, shuff=False, shape=(240, 144, 240))

model = get_usscnet()



model.compile(optimizer=SGD(lr=LR, decay=DECAY,  momentum=0.9),
              loss=weighted_categorical_crossentropy,
              metrics=[comp_iou, seg_iou, nclasses])

model_name= 'usscnet'+ \
            '_LR' + str(LR) +\
            '_DC' + str(DECAY)

run = get_run()
model_name_run = model_name + "_" + str(run)

print("Model:", model_name_run)


#model.load_weights('sscnet_LR0.01_DC0.0005_36.hdf5')
#model.load_weights('sscnet_LR0.1_DC0.0005_84.hdf5')
model.fit_generator(
        train_datagen,
        steps_per_epoch=np.ceil(len(train_prefixes)//BATCH_SIZE),
        epochs=30,
        validation_data=val_datagen,
        validation_steps=np.ceil(len(val_prefixes)//VAL_BATCH_SIZE),
        callbacks=get_callbacks(model_name_run, log_dir="log", patience=3),
        workers=6 #,
        #class_weight=weights
        )
