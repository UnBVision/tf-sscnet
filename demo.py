DEVICE = 0

import os
os.environ["KERAS_BACKEND"]="tensorflow"
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]=str(DEVICE)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from keras.optimizers import SGD


from lib_sscnet.py_cuda import *
from lib_sscnet.losses import *
from lib_sscnet.network import get_sscnet
from lib_sscnet.voxel_plot import *

import argparse


###################################
# Default parameters
###################################
IMAGE_DIR = './sample_images'
WEIGHTS_DIR = './weights'
WEIGHTS_FILE = 'tf-sscnet_nyu.hdf5'
FILE_PREFIX = ''
###################################

def run():

    model = get_sscnet()
    model.compile(optimizer=SGD(), loss=weighted_categorical_crossentropy)
    model.load_weights(os.path.join(WEIGHTS_DIR,WEIGHTS_FILE))


    voxel_shape = (240, 144, 240)

    class_names = get_class_names()

    ##################################


    print("Preprocessing...")
    lib_sscnet_setup(device=DEVICE, num_threads=128, K=None, frame_shape=(640, 480), v_unit=0.02, v_margin=0.24)
    vox_ftsdf, vox_gt, vox_flags = process_evaluate(FILE_PREFIX, voxel_shape, down_scale = 4)

    x = vox_ftsdf.reshape((1, voxel_shape[0], voxel_shape[1], voxel_shape[2],1))

    vox_gt = vox_gt.reshape((60, 36, 60))
    vox_flags = vox_flags.reshape((60, 36, 60))


    print("Predicting...")
    pred = model.predict(x=x)
    pred_classes = np.argmax(pred, axis=-1)

    if SHOW_GT:
        print("Preparing GT visualization...")
        plt1, ax1 = plot_ground_truth(vox_gt)

    if SHOW_PRED:
        print("Preparing prediction visualization...")
        plt2, ax2 = plot_pred(pred_classes[0], vox_flags)


    if SHOW_GT:
        plt1.show()
    if SHOW_PRED:
        plt2.show()


def parse_arguments():

    global IMAGE_DIR, FILE_PREFIX, SHOW_PRED, SHOW_GT, WEIGHTS_DIR, WEIGHTS_FILE

    print("\ntf-sscnet demo\n")


    parser = argparse.ArgumentParser()
    parser.add_argument("scene", help="Scene to process without extension.", type=str)
    parser.add_argument("--image_dir", help="Image directory.", type=str, default=IMAGE_DIR, required=False)
    parser.add_argument("--weights_dir", help="Weights directory.", type=str, default=WEIGHTS_DIR, required=False)
    parser.add_argument("--weights_file", help="Weights file.", type=str, default=WEIGHTS_FILE, required=False)
    parser.add_argument("--show_pred", help="If it is to show prediction.", type=str, default="Y", required=False,choices=['y','Y', 'n', 'N'])
    parser.add_argument("--show_gt", help="If it is to show groud truth.", type=str, default="N", required=False,choices=['y','Y', 'n', 'N'])
    args = parser.parse_args()

    IMAGE_DIR = args.image_dir
    FILE_PREFIX = os.path.join(IMAGE_DIR, args.scene)
    WEIGHTS_DIR = args.weights_dir
    WEIGHTS_FILE =args.weights_file
    SHOW_PRED = args.show_pred.upper() in ['YES', 'Y']
    SHOW_GT = args.show_gt.upper() in ['YES', 'Y']

    print("Processing scene:", FILE_PREFIX)
    print("Weight file:", os.path.join(WEIGHTS_DIR, WEIGHTS_FILE))

# Main Function
if __name__ == '__main__':
  parse_arguments()
  run()

