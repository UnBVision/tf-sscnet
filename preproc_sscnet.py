import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="0"
os.environ["KERAS_BACKEND"]="tensorflow"
os.environ['KERAS_BACKEND'] = 'tensorflow'
from lib_sscnet.file_utils import *
import numpy as np # linear algebra
import multiprocessing
import argparse
import time
import random



##################################
# Parameters
##################################

BASE_PATH = '/home/adn/sscnet/data'
DATASET = 'SUNCGtrain'

#BASE_PATH = '/home/adn/sscnet/data/depthbin'
#DATASET = 'SUNCGtest_49700_49884'

DEST_PATH = '/d02/data/sscnet_preproc'

DEVICE = 0

THREADS = 4

# Globals
proc_prefixes = multiprocessing.Value('i',0)
total_prefixes = 0
processed = 0


##################################

def parse_arguments():
    global BASE_PATH, DATASET, DEST_PATH, DEVICE, THREADS

    print("\nSSCNET PREPROCESSOR\n")

    parser = argparse.ArgumentParser()
    parser.add_argument("dataset", help="Directory (related to base_path), of the original data", type=str)
    parser.add_argument("--base_path", help="Base path of the original data. Default: "+BASE_PATH, type=str, default=BASE_PATH, required=False)
    parser.add_argument("--dest_path", help="Destination path. Default: "+DEST_PATH, type=str, default=DEST_PATH, required=False)
    parser.add_argument("--device", help="CUDA device. Default " + str(DEVICE), type=int, default=DEVICE, required=False)
    parser.add_argument("--threads", help="Concurrent threads. Default " + str(THREADS), type=int, default=THREADS, required=False)
    args = parser.parse_args()

    BASE_PATH = args.base_path
    DATASET = args.dataset
    DEST_PATH = args.dest_path
    DEVICE = args.device
    THREADS = args.threads

def process_multi(file_prefix):

    global BASE_PATH, DATASET, DEST_PATH, DEVICE, THREADS, to_process, proc_prefixes, total_time, processed


    if file_prefix not in  ["/home/adn/sscnet/data/SUNCGtrain/SUNCGtrain_5001_7000/00001159_12d1e69af34ba4eb1a943506a150eb3c_fl001_rm0001_0000",
                            "/home/adn/sscnet/data/SUNCGtrain/SUNCGtrain_5001_7000/00001160_12d1e69af34ba4eb1a943506a150eb3c_fl001_rm0001_0000",
                            "/home/adn/sscnet/data/SUNCGtrain/SUNCGtrain_3001_5000/00048897_0ea5eadf991d8ed02ddf5a5c007f1a01_fl001_rm0008_0000",
                            "/home/adn/sscnet/data/SUNCGtrain/SUNCGtrain_3001_5000/00016521_17da926b54ab4a91126d43718dc2e9a2_fl001_rm0006_0000",
                            "/home/adn/sscnet/data/SUNCGtrain/SUNCGtrain_3001_5000/00036189_0814b0aaa416eeb1394735d74d4b7a36_fl002_rm0002_0000",
                            "/home/adn/sscnet/data/SUNCGtrain/SUNCGtrain_3001_5000/00016524_17da926b54ab4a91126d43718dc2e9a2_fl001_rm0006_0000",
                            "/home/adn/sscnet/data/SUNCGtrain/SUNCGtrain_3001_5000/00036190_0814b0aaa416eeb1394735d74d4b7a36_fl002_rm0002_0000",
                            "/home/adn/sscnet/data/SUNCGtrain/SUNCGtrain_5001_7000/00001161_12d1e69af34ba4eb1a943506a150eb3c_fl001_rm0001_0000",
                            "/home/adn/sscnet/data/SUNCGtrain/SUNCGtrain_3001_5000/00048896_0ea5eadf991d8ed02ddf5a5c007f1a01_fl001_rm0008_0000"]:

        lib_sscnet_setup(device=DEVICE, num_threads=128, K=None, frame_shape=(640, 480), v_unit=0.02, v_margin=0.24)

        dest_prefix = DEST_PATH + file_prefix[len(BASE_PATH):]
        preproc_file = dest_prefix + '.npz'

        directory = os.path.split(dest_prefix)[0]

        with proc_prefixes.get_lock():
            if not os.path.exists(directory):
                os.makedirs(directory)

        shape = (240, 144, 240)

        vox_tsdf, segmentation_label, vox_weights = process(file_prefix, voxel_shape=shape, down_scale=4)

        down_shape=(shape[0]//4, shape[1]//4, shape[2]//4 )

        if np.sum(segmentation_label) == 0:
            print(file_prefix)
            exit(-1)
        else:

            np.savez_compressed(preproc_file,
                           tsdf=vox_tsdf.reshape((shape[0], shape[1], shape[2], 1)),
                           lbl=segmentation_label.reshape((down_shape[0], down_shape[1], down_shape[2], 1)),
                           weights= vox_weights.reshape((down_shape[0], down_shape[1], down_shape[2])))

    with proc_prefixes.get_lock():
        proc_prefixes.value += 1

        counter = proc_prefixes.value
        mean_time = (time.time() - total_time)/counter
        eta_time = mean_time * (to_process - counter)
        eta_h = eta_time // (60*60)
        eta_m = (eta_time - (eta_h*60*60))//60
        eta_s = eta_time - (eta_h*60*60) - eta_m * 60
        perc = 100 * counter/to_process

        print("  %3.2f%%  Mean Processing Time: %.2f seconds ETA: %02d:%02d:%02d     " % (perc, mean_time, eta_h, eta_m, eta_s), end="\r")





# Main Function
def Run():
    global BASE_PATH, DATASET, DEST_PATH, DEVICE, THREADS, to_process, proc_prefixes, total_time
    parse_arguments()

    print(" .")
    processed_files = get_file_prefixes_from_path(os.path.join(DEST_PATH, DATASET), criteria='*.npz')

    print(" ..")
    processed_files = [os.path.split(x)[1] for x in processed_files]


    data_path = os.path.join(BASE_PATH, DATASET)

    print("Data path %s" % data_path)
    all_prefixes = get_file_prefixes_from_path(data_path)

    file_prefixes = [x for x in all_prefixes if os.path.split(x)[1] not in processed_files]
    to_process = len(file_prefixes)

    print("Files: %d. Already processed: %d. To process: %d.\n" % (len(all_prefixes), len(processed_files), to_process))

    total_time = time.time()

    if to_process>0:
        pool = multiprocessing.Pool(processes=THREADS)

        pool.map(process_multi, file_prefixes)
        pool.close()
        pool.join()

    print("Total time: %s seconds                                                       " % (time.time() - total_time))

if __name__ == '__main__':
  Run()